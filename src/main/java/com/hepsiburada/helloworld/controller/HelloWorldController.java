package com.hepsiburada.helloworld.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/")
    public String helloResource(){

        return new String("Hello Hepsiburada from Kaan Diren");
    }
}
