#
# Build stage
#
FROM maven:3.5-jdk-8 AS build
LABEL author="kaandiren"
COPY src /Users/kaan/Desktop/helloworld/src
COPY pom.xml /Users/kaan/Desktop/helloworld
RUN mvn -f /Users/kaan/Desktop/helloworld/pom.xml clean package

#
# Package stage
#
FROM gcr.io/distroless/java
COPY --from=build /Users/kaan/Desktop/helloworld/target/helloworld-0.0.1-SNAPSHOT.jar /Users/kaan/Desktop/helloworld/helloworld-0.0.1-SNAPSHOT.jar
EXPOSE 11130
ENTRYPOINT ["java","-jar","/Users/kaan/Desktop/helloworld/helloworld-0.0.1-SNAPSHOT.jar"]